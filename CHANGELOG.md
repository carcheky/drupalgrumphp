## [2.0.2](https://gitlab.com/carcheky/drupalgrumphp/compare/v2.0.1...v2.0.2) (2023-06-14)


### Bug Fixes

* changed 'code' to 'commit' in output messages ([6b380b5](https://gitlab.com/carcheky/drupalgrumphp/commit/6b380b5de6ef75094c979532611820fe07c18d2b))

## [2.0.1](https://gitlab.com/carcheky/drupalgrumphp/compare/v2.0.0...v2.0.1) (2023-06-14)


### Bug Fixes

* changed output messages ([af3eb10](https://gitlab.com/carcheky/drupalgrumphp/commit/af3eb10e6e81f705e3954a1dcc6824388627bbaf))

# [2.0.0](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.2.5...v2.0.0) (2023-06-14)


### Features

* drupal 10 ready ([98c485a](https://gitlab.com/carcheky/drupalgrumphp/commit/98c485ae5a1aedea28a3e80f96d37579fa9d96c7))


### BREAKING CHANGES

* drupal 10 ready

## [1.2.5](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.2.4...v1.2.5) (2023-01-30)


### Bug Fixes

* **phpcs:** remove js checks ([309abf6](https://gitlab.com/carcheky/drupalgrumphp/commit/309abf6122c577d65de093f6a6d7fd40627052d4))

## [1.2.4](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.2.3...v1.2.4) (2022-12-01)


### Bug Fixes

* updated phpstan ([86fb9f7](https://gitlab.com/carcheky/drupalgrumphp/commit/86fb9f74d21d788e4a5b50e2898d0c271f288b1a))

## [1.2.3](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.2.2...v1.2.3) (2022-11-29)


### Bug Fixes

* allow merge message ([69c6231](https://gitlab.com/carcheky/drupalgrumphp/commit/69c6231d298e1283b94ad72c5bf425a4c9417b63))

## [1.2.2](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.2.1...v1.2.2) (2022-10-05)


### Bug Fixes

* pre-commit launcher ([0e492a9](https://gitlab.com/carcheky/drupalgrumphp/commit/0e492a93cef948e5626e1f37fe39a1304942dd23))

## [1.2.1](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.2.0...v1.2.1) (2022-10-05)


### Bug Fixes

* remove npm dependencies ([4952dae](https://gitlab.com/carcheky/drupalgrumphp/commit/4952daec5c69ebcb04b384ed8329a6a618569212))

# [1.2.0](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.1.20...v1.2.0) (2022-10-05)


### Features

* removed stylelint ([a6e6fa8](https://gitlab.com/carcheky/drupalgrumphp/commit/a6e6fa80adb22871d88d2ac62632b37aadd8f8ef))

## [1.1.20](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.1.19...v1.1.20) (2022-09-07)


### Bug Fixes

* updated changelog ([8501435](https://gitlab.com/carcheky/drupalgrumphp/commit/8501435e03545df824475d1f4c63863a95f872ca))

## [1.1.19](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.1.18...v1.1.19) (2022-09-07)


### Bug Fixes

* exclude .releaserc from build ([0c8d1e5](https://gitlab.com/carcheky/drupalgrumphp/commit/0c8d1e55830a31f0e05af1f73e6cf7b94746160f))

## [1.1.18](https://gitlab.com/carcheky/drupalgrumphp/compare/v1.1.17...v1.1.18) (2022-09-07)


### Bug Fixes

* added simple change to test semantic release ([25f7577](https://gitlab.com/carcheky/drupalgrumphp/commit/25f7577f89bd72935c9a57eb1fdb787fe168a00d))
* added simple change to test semantic release ([bc97241](https://gitlab.com/carcheky/drupalgrumphp/commit/bc972410d42c84c585574001e683a01dfd249185))
* added versions ([cd2ffa4](https://gitlab.com/carcheky/drupalgrumphp/commit/cd2ffa4facce967cd027ce5e17d731a45a48e09d))
